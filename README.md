## my task
The task is split into two parts:
1. Create two models, one for sperm cells detection and second for sperm cell bundle formation detection in a microscope image.
2. Create a program that will use earlier trained models to detect sperm cells detection and sperm cell bundle formation detection in a video.


## what did i read?
- [ ] [Lets use Active Learning to train YoloV5 on Autonomous driving data.](https://medium.com/@bhaskarbose1998/lets-use-active-learning-to-train-yolov5-on-autonomous-driving-data-b2c5469e83bd)
- [ ] [How to Detect Objects in Images Using the YOLOv8 Neural Network](https://www.freecodecamp.org/news/how-to-detect-objects-in-images-using-yolov8/)
- [ ] [YOLOv8 for Object Detection Explained [Practical Example]](https://medium.com/cord-tech/yolov8-for-object-detection-explained-practical-example-23920f77f66a)
- [ ] [YOLOv8 Tutorial](https://colab.research.google.com/github/ultralytics/ultralytics/blob/main/examples/tutorial.ipynb#scrollTo=ktegpM42AooT)
- [ ] [Train YOLOv8 Instance Segmentation on Your Data](https://towardsdatascience.com/trian-yolov8-instance-segmentation-on-your-data-6ffa04b2debd)

## repository structure
```
├── JSON2YOLO(program for converting JSON COCO1.0 to YOLO format)
├── yolo8spermTraining.ipynb - training yolov8 models in google colab
├── models_training_pipeline.ipynb - notebook where i extract frames from input videos, create training datasets and validate models
├── simple_bundle_detector.ipynb - notebook where i detect sperm cell bundles in video. 
├── bundle_detector.ipynb - notebook where i detect not only sperm cell bundles but their formation and separation in video using advanced technique. 
├── output - folder with output videos
├── models - folder with detection models
```
## instructions
to run bundle detection on video file:
1. upload video file to root folder
2. set input file path in last notebook cell 
3. run simple_bundle_detector.ipynb or bundle_detector.ipynb
4. openCV will open video window with detection results
5. output will be saved in root folder

